-- 데이터 분석 팀에서는 우유(Milk)와 요거트(Yogurt)를 동시에 구입한 장바구니가 있는지 알아보려 합니다.
-- 우유와 요거트를 동시에 구입한 장바구니의 아이디를 조회하는 SQL 문을 작성해주세요.
-- 이때 결과는 장바구니의 아이디 순으로 나와야 합니다.

-- CART_PRODUCTS 테이블 구조
NAME      TYPE      DESC
ID        INT       제품 ID
CART_ID   INT       제품이 담긴 CART의 ID
NAME      VARCHAR   제품 이름
PRICE     INT       제품 가격

# SELECT
#     DISTINCT CART_ID
# FROM CART_PRODUCTS AS CP
# WHERE EXISTS(SELECT * FROM CART_PRODUCTS WHERE CP.CART_ID = CART_ID AND NAME = 'Milk')
# AND EXISTS(SELECT * FROM CART_PRODUCTS WHERE CP.CART_ID = CART_ID AND NAME = 'Yogurt');

SELECT
    CART_ID
FROM (
    SELECT
        CART_ID,
        CASE
            WHEN NAME = 'Milk'
            THEN 1

            WHEN NAME = 'Yogurt'
            THEN 2

            ELSE 0
        END AS FLAG
    FROM CART_PRODUCTS
    WHERE NAME IN ('Milk', 'Yogurt')
) AS P
GROUP BY P.CART_ID
HAVING BIT_OR(P.FLAG) = 3;
