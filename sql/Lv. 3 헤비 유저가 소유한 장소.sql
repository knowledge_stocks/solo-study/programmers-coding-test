-- 이 서비스에서는 공간을 둘 이상 등록한 사람을 "헤비 유저"라고 부릅니다.
-- 헤비 유저가 등록한 공간의 정보를 아이디 순으로 조회하는 SQL문을 작성해주세요.

-- PLACES 테이블 구조
-- NAME      TYPE      DESC
-- ID        INT       공간의 ID
-- NAME      VARCHAR   공간의 이름
-- HOST_ID   INT       공간을 등록한 사람의 ID

SELECT
    ID,
    NAME,
    HOST_ID
FROM PLACES
WHERE HOST_ID IN (
    SELECT
        HOST_ID
    FROM PLACES
    GROUP BY HOST_ID
    HAVING COUNT(HOST_ID) >= 2
)
