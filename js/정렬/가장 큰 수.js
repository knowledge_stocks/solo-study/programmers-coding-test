function solution(numbers) {
    var sorted = numbers.sort(function(l,r) {
        return ('' + r + l).localeCompare('' + l + r);
    });
    return sorted[0] === 0 ? '0' : sorted.join('');
}

console.log(solution([6, 10, 2]));