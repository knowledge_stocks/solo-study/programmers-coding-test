function solution(citations) {
    citations.sort(function(l, r) {
        return r - l;
    });

    // [6,5,3,1,0]
    // [1,2,3]
    for(var i = 0; i < citations.length; i++) {
        if(citations[i] <= i)
        {
            return i;
        }
    }
    return i;
}

console.log(solution([3, 0, 6, 1, 5]));