function combination(arr, size) {
    if(size > arr.length || size < 1) {
        return [];
    } else if(size === 1) {
        return arr.map(v => [v]);
    }

    let result = [];

    arr.forEach((v, idx) => {
        let subCombinations = combination(arr.slice(idx + 1), size - 1);
        let newCombinations = subCombinations.map(v2 => [v, ...v2]);
        result.push(...newCombinations);
    });

    return result;
}

console.log(combination([1,2,3,4], 3));
// 결과
// [ [ 1, 2, 3 ], [ 1, 2, 4 ], [ 1, 3, 4 ], [ 2, 3, 4 ] ]