function permutation(arr, size) {
    if(size > arr.length || size < 1) {
        return [];
    } else if(size === 1) {
        return arr.map(v => [v]);
    }

    let result = [];

    arr.forEach((v, idx) => {
        let subPermutations = permutation([...arr.slice(0, idx), ...arr.slice(idx + 1)], size - 1);
        let newPermutations = subPermutations.map(v2 => [v,...v2]);
        result.push(...newPermutations);
    });

    return result;
}

console.log(permutation([1,2,3,4], 3));
// 결과
// [
//   [ 1, 2, 3 ], [ 1, 2, 4 ],
//   [ 1, 3, 2 ], [ 1, 3, 4 ],
//   [ 1, 4, 2 ], [ 1, 4, 3 ],
//   [ 2, 1, 3 ], [ 2, 1, 4 ],
//   [ 2, 3, 1 ], [ 2, 3, 4 ],
//   [ 2, 4, 1 ], [ 2, 4, 3 ],
//   [ 3, 1, 2 ], [ 3, 1, 4 ],
//   [ 3, 2, 1 ], [ 3, 2, 4 ],
//   [ 3, 4, 1 ], [ 3, 4, 2 ],
//   [ 4, 1, 2 ], [ 4, 1, 3 ],
//   [ 4, 2, 1 ], [ 4, 2, 3 ],
//   [ 4, 3, 1 ], [ 4, 3, 2 ]
// ]