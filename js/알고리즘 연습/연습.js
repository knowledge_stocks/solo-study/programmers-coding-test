//조합
function combination(arr, size) {
    if(size > arr.length || size < 1) {
        return [];
    } else if(size === 1) {
        return arr.map(v => [v]);
    }

    let result = [];
    arr.forEach((v, idx, origin) => {
        let subCombis = combination(origin.slice(idx + 1), size - 1);
        let newCombis = subCombis.map(v2 => [v, ...v2]);
        result.push(...newCombis);
    })

    return result;
}
// console.log(combination([1,2,3,4], 3));

// 순열
function permutation(arr, size) {
    if(size > arr.length || size < 1) {
        return [];
    } else if(size === 1) {
        return arr.map(v => [v]);
    }

    let result = [];
    arr.forEach((v, idx, origin) => {
        let subPermu = permutation([...origin.slice(0, idx), ...origin.slice(idx+1)], size - 1);
        let newPermu = subPermu.map(v2 => [v, ...v2]);
        result.push(...newPermu);
    })

    return result;
}
// console.log(permutation([1,2,3,4], 3));

function quickSort(arr) {
    return recur(0, arr.length - 1);

    function recur(left, right) {
        if(left >= right) {
            return;
        }

        let originL = left;
        let originR = right;
        let pivot = arr[Math.floor((left + right) / 2)];

        while(left <= right) {
            while(arr[left] < pivot) {
                left++;
            }
            while(arr[right] > pivot) {
                right--;
            }

            if(left <= right) {
                [arr[left], arr[right]] = [arr[right], arr[left]];
                left++;
                right--;
            }
        }

        recur(originL, left - 1);
        recur(left, originR);

        return arr;
    }
}

// console.log(quickSort([6,3,8,5,2]));
// console.log(quickSort([5, 8, 1, 3, 9, 4, 7]));
// console.log(quickSort([1, 2, 3, 4, 5, 6, 7]));

// 이진탐색
function binarySearch(arr, v) {
    let left = 0;
    let right = arr.length - 1;

    while(left <= right) {
        let mid = Math.floor(left + right)
    }
}