function binarySearchIndex(arr, v) {
    let left = 0;
    let right = arr.length - 1;

    while(left < right)  {
        let average = Math.floor((left + right) / 2);

        if(arr[average] > v) {
            right = average - 1;
        } else if(arr[average] < v) {
            left = average + 1;
        } else {
            right = average; // 같은 값일 경우 최소 인덱스를 리턴하기 위함
        }
    }

    if(arr[left] === v) {
        return left;
    }
    return -1;
}

console.log(binarySearchIndex([2,3,5,6,6,8], 6));
console.log(binarySearchIndex([2,3,3,5,6,8], 3));
console.log(binarySearchIndex([2,3,5,6,8,8], 8));
console.log(binarySearchIndex([2,3,5,5,6,8], 5));
console.log(binarySearchIndex([2,2,3,5,6,8], 2));
console.log(binarySearchIndex([2,3,5,6,8], 0));
console.log(binarySearchIndex([2,3,5,6,8], 4));
console.log(binarySearchIndex([2,3,5,6,8], 7));
console.log(binarySearchIndex([2,3,5,6,8], 10));
console.log(binarySearchIndex([1,3,4,5,7,7,8,9], 7));
console.log(binarySearchIndex([1,3,4,5,7,8,9], 6));
console.log(binarySearchIndex([1, 2, 3, 4, 4, 5, 6, 7], 4));