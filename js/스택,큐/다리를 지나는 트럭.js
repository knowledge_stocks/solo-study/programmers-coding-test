function solution(bridge_length, weight, truck_weights) {
    var time = 0;
    var queue = new Array(bridge_length).fill(0);
    var on_bridge_weight = 0;
    while(truck_weights.length > 0 || on_bridge_weight > 0) {
        time++;
        on_bridge_weight -= queue.pop();
        if(on_bridge_weight + truck_weights[0] <= weight) {
            queue.unshift(truck_weights.shift());
            on_bridge_weight += queue[0];
        } else {
            queue.unshift(0);
        }
    }

    return time;
}

console.log(solution(2, 10, [7,4,5,6]));