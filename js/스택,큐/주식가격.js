function solution(prices) {
    var result = [];

    prices.forEach(function(price, idx) {
        var count = 0;
        for(var i = idx + 1; i < prices.length; i++) {
            count++;
            if(price > prices[i]) {
                break;
            }
        }
        result.push(count);
        return
    })

    return result;
}


console.log(solution([1, 2, 3, 2, 3]));