function solution(answers) {
    var supo_patters = [];
    supo_patters[0] = [1, 2, 3, 4, 5];
    supo_patters[1] = [2, 1, 2, 3, 2, 4, 2, 5];
    supo_patters[2] = [3, 3, 1, 1, 2, 2, 4, 4, 5, 5];

    var supo_points = new Array(3).fill(0);

    for(var i = 0; i < answers.length; i++) {
        for(var j = 0; j < supo_patters.length; j++) {
            supo_points[j] += supo_patters[j][i % supo_patters[j].length] === answers[i];
        }
    }

    var best_point = Math.max(...supo_points);
    var answer = [];
    for(var i = 0; i < supo_points.length; i++) {
        if(best_point === supo_points[i]) {
            answer.push(i+1);
        }
    }

    return answer;
}

console.log(solution([1,2,3,4,5]));
console.log(solution([1,3,2,4,2]));