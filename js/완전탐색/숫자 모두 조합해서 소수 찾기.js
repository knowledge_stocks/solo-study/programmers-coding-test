function solution(numbers) {
    var zenNumbers = new Set();

    zen('', numbers.split(''));

    return [...zenNumbers].filter(function(v) {
        if(v < 2) {
            return false;
        }
        for(var i = 2; i <= Math.sqrt(v); i++) {
            if(v % i === 0) {
                return false;
            }
        }
        return true;
    }).length;

    function zen(v, nums) {
        for(var i = 0; i < nums.length; i++) {
            var newNums = nums.slice();
            newNums.splice(i, 1);
            var newVal = v + nums[i];
            zenNumbers.add(newVal * 1);
            zen(newVal, newNums);
        }
    }
}

console.log(solution("17"));
console.log(solution("011"));