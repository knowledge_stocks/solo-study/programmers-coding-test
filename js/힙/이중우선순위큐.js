function solution(operations) {
    var values = [];

    for(var oper of operations) {
        var opers = oper.split(' ');

        switch(opers[0]) {
            case 'I' :
                values.unshift(opers[1] * 1);
                values.sort((l,r) => l - r);
                break;
            case 'D' :
                opers[1] === '1' ? values.pop() : values.shift();
                break;
        }
    }
    if(values.length) {
        return [ values[values.length - 1], values[0] ]
    }
    return [0,0]
}

console.log(solution(["I 16","D 1"]));
console.log(solution(["I 7","I 5","I -5","D -1"]));