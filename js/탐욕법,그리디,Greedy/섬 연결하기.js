// function solution(n, costs) {
//     var isConnected = new Array(n).fill(false); // 0번이랑 연결되어 있는지 여부
//     isConnected[0] = true;

//     var costSum = 0;
//     for(var i = 0; i < n; i++) {
//         var myCosts = costs.map((v,i) => {
//             v.idx = i;
//             return v;
//         }).filter(v => v[0] === i || v[1] === i);

//         if(!isConnected[i]) {
//             var connectedCosts = myCosts.filter((v) => isConnected[v[0]] || isConnected[v[1]]);
//             var minCosts = undefined;
//             for(var j = 0; j < connectedCosts.length; j++) {
//                 if(minCosts === undefined || minCosts[2] > connectedCosts[j][2]) {
//                     minCosts = connectedCosts[j];
//                 }
//             }
//             if(minCosts !== undefined) {
//                 costs.splice(minCosts.idx, 1);
//                 costSum += minCosts[2];
//                 isConnected[i] = true;
//                 continue;
//             }
//         }

//         var targetCosts = myCosts.filter((v) => (v[0] === i ? !isConnected[v[1]] : !isConnected[v[0]]));
//         var minCosts = undefined;
//         for(var j = 0; j < targetCosts.length; j++) {
//             if(minCosts === undefined || minCosts[2] > targetCosts[j][2]) {
//                 minCosts = targetCosts[j];
//             }
//         }
//         if(minCosts !== undefined) {
//             costs.splice(minCosts.idx, 1);
//             costSum += minCosts[2];
//             isConnected[minCosts[0]] = true;
//             isConnected[minCosts[1]] = true;
//             continue;
//         }
//     }

//     return costSum;
// }

function solution(n, costs) {
    var isConnected = new Array(n).fill(false); // 0번이랑 연결되어 있는지 여부
    var connectionCount = 0;

    var costSum = 0;
    while(connectionCount < n - 1) {
        var targetCosts = costs.filter((v) => (connectionCount === 0 || (isConnected[v[1]] ^ isConnected[v[0]])));
        var minCosts = undefined;
        for(var j = 0; j < targetCosts.length; j++) {
            if(minCosts === undefined || minCosts[2] > targetCosts[j][2]) {
                minCosts = targetCosts[j];
            }
        }
        connectionCount++;
        costSum += minCosts[2];
        isConnected[minCosts[0]] = true;
        isConnected[minCosts[1]] = true;
    }

    return costSum;
}

console.log(solution(4,[[0,1,1],[0,2,2],[1,2,5],[1,3,1],[2,3,8]],4));