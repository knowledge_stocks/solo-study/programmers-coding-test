function solution(n, lost, reserve) {
    lost.sort((l,r) => l-r);
    reserve.sort((l,r) => l-r);
    for(var i = 0; i < lost.length; i++) {
        for(var j=0; j < reserve.length; j++) {
            if(lost[i] === reserve[j]) {
                lost.splice(i,1);
                reserve.splice(j,1);
            }
        }
    }

    var result = n - lost.length;
    for(var i = 0; i < lost.length; i++) {
        var reserverIdx = reserve.findIndex((v) => (lost[i] - 1 === v) || (lost[i] === v) ||  (lost[i] + 1 === v));
        if(reserverIdx != -1) {
            reserve.splice(reserverIdx, 1);
            result++;
        }
    }
    return result;
}

console.log(solution(5,[2, 4],[1, 3, 5],5));
console.log(solution(5,[2, 4],[3],4));
console.log(solution(3,[3],[1],2));