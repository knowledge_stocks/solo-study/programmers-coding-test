function solution(number, k) {
    var j = 0;
    for(var i = 0; i < k; i++) {
        for(; j < number.length - 1; j++) {
            if(number[j] === 0 ||  number[j] < number[j+1]) {
                number = number.substring(0, j) + number.substring(j + 1, number.length); // 여기서 시간을 많이 잡아먹음
                j--;
                break;
            }
        }
        if(j === number.length - 1) {
            number = number.substring(0, number.length - k);
            break;
        }
    }
    return number;
}

function solution2(number, k) {
    var numbers = [...number];
    var j = 0;

    for(var i = 0; i < k; i++) {
        for(; j < numbers.length - 1; j++) {
            if(numbers[j] === 0 ||  numbers[j] < numbers[j+1]) {
                numbers.splice(j, 1); // 이게 더 시간을 많이 잡아먹음
                j--;
                break;
            }
        }
        if(j === numbers.length - 1) {
            numbers = numbers.slice(0, numbers.length - k);
            break;
        }
    }
    return numbers.join('');
}

function solution3(number, k) {
    const stack = [];
    let answer = '';

    for(let i=0; i<number.length; i++){
      const el = number[i];

      while(k > 0 && stack[stack.length-1] < el){
        stack.pop();
        k--;
      }
      stack.push(el);
    }

    stack.splice(stack.length-k, k);
    answer = stack.join("");
    return answer;
}

console.log(solution("1924",2,"94"));
console.log(solution2("1924",2,"94"));
console.log(solution("1231234",3,"3234"));
console.log(solution2("1231234",3,"3234"));
console.log(solution("4177252841",4,"775841"));
console.log(solution2("4177252841",4,"775841"));
console.log(solution("1924",3,"9"));
console.log(solution2("1924",3,"9"));