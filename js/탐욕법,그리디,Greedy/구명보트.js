function solution(people, limit) { // 정답
    people.sort((l,r) => r - l);

    var count = 0;

    var l = 0;
    var r = people.length - 1;
    while(l < r) {
        if(people[r] > limit / 2) {
            count += r - l + 1;
            break;
        }

        l++;
        if(people[l - 1] + people[l] <= limit) {
            l++;
        } else if(people[l - 1] + people[r] <= limit) {
            r--;
        }
        count++;
    }
    if(l===r) {
        count++;
    }
    return count;
}

function solution2(people, limit) { // 시간초과
    people.sort((l,r) => r - l);

    var count = 0;

    while(people.length > 0) {
        if(people[people.length - 1] > limit / 2) {
            count += people.length;
            break;
        }

        var p1 = people.shift();
        if(p1 + people[0] <= limit) {
            people.shift();
        } else if(p1 + people[people.length - 1] <= limit) {
            people.pop();
        }
        count++;
    }
    return count;
}

console.log(solution([70, 50, 80, 50],100,3));
console.log(solution([70, 80, 50],100,3));