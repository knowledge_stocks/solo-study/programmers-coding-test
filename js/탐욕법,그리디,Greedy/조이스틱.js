function solution(name) {
    var text = name.replace(/\w/g, 'A');

    var count = 0;
    var position = 0;
    while(text !== name) {
        var targetCode = name.charCodeAt(position);
        if(targetCode <= 78) { // 78 = 'N'
            count += targetCode - text.charCodeAt(position);
        }
        else {
            count += 26 + text.charCodeAt(position) - targetCode;
        }
        text = text.substring(0, position) + name[position] + text.substring(position + 1, text.length);

        if(text !== name) {
            for(var rightDist = 1; rightDist <= text.length / 2; rightDist++) {
                if(text[position + rightDist] !== name[position + rightDist]){
                    break;
                }
            }
            for(var leftDist = 1; leftDist <= text.length / 2; leftDist++) {
                if(text[(text.length + position - leftDist) % text.length] !== name[(text.length + position - leftDist) % text.length]){
                    break;
                }
            }
            if(rightDist <= leftDist) {
                position += rightDist;
                count += rightDist;
            } else {
                position = (text.length + position - leftDist) % text.length;
                count += leftDist;
            }
        }
    }
    return count;
}

console.log(solution("JEROEN",56));
console.log(solution("JAN",23));
console.log(solution("ABABAAAAABA" ,10)); // 10이 나오는 것이 최적의 해인데 11이 나옴