function solution(n, computers) {
    var is_visited = new Array(computers.length).fill(false);
    var count = 0;
    for(var i = 0; i < computers.length; i++) {
        count += visit_recur(i, is_visited, computers);
    }
    return count;
}

function visit_recur(idx, is_visited, computers) {
    if(is_visited[idx]) {
        return 0;
    }
    is_visited[idx] = true;
    for(var i = 0; i < computers[idx].length; i++) {
        if(computers[idx][i]) {
            visit_recur(i, is_visited, computers);
        }
    }
    return 1;
}


console.log(solution(3,[[1, 1, 0], [1, 1, 0], [0, 0, 1]]));
console.log(solution(3,[[1, 1, 0], [1, 1, 1], [0, 1, 1]]));