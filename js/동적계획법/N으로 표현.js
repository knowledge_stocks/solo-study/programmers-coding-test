function solution(N, number) {
    var cache = [];

    for(var i = 1; i < 9; i++) {
        cache[i] = [ Number.parseInt(new Array(i).fill(N).join('')) ];
        for(var j = 1; j < i; j++) {
            cache[i - j].forEach(function(op1) {
                cache[j].forEach(function(op2) {
                    cache[i].push(op1 + op2);
                    cache[i].push(op1 - op2);
                    cache[i].push(op1 * op2);
                    if(op2) {
                        cache[i].push(Math.floor(op1 / op2));
                    }
                });
            });
        }
        if(cache[i].includes(number)) {
            return i;
        }
    }

    return -1;
}

console.log(solution(5, 12));
console.log(solution(2, 11));