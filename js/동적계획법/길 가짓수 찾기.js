function solution(m, n, puddles) {
    var map = [];
    for(var i = 0; i < m+1; i++) {
        map[i] = new Array(n+1).fill(0);
    }

    map[0][1] = 1;
    for(var axis of puddles) {
        map[axis[0]][axis[1]] = undefined;
    }

    for(var i = 1; i < map.length; i++) {
        for(var j = 1; j < map[i].length; j++) {
            if(map[i][j] !== undefined) {
                map[i][j] = (map[i - 1][j]>>0) + (map[i][j - 1]>>0);
            }
        }
    }
    return map[m][n];
}

console.log(solution(4, 3, [[2,2]]));
console.log(solution(4, 4, [[3,2], [2,4]]));