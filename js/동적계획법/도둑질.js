function solution(money) {
    var except_first_money = money.slice(1);
    var except_first_accum = [ except_first_money[0] ];

    for(var i = 1; i < except_first_money.length; i++) {
        except_first_accum[i] = Math.max((except_first_accum[i-1]>>0), (except_first_accum[i-2]>>0) + except_first_money[i]);
    }

    var except_last_money = money.slice(0, money.length - 1);
    var except_last_accum = [ except_last_money[0] ];

    for(var i = 1; i < except_last_money.length; i++) {
        except_last_accum[i] = Math.max((except_last_accum[i-1]>>0), (except_last_accum[i-2]>>0) + except_last_money[i]);
    }

    return Math.max(except_first_accum[except_first_accum.length - 1], except_last_accum[except_last_accum.length - 1]);
}

console.log(solution([1, 2, 3, 1]));
