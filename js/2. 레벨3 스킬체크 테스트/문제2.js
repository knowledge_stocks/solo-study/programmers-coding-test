function solution(user_id, banned_id) {
    if(banned_id.length == 0) {
        return 0;
    }

    var cache = [ { peek: [], notPeek: user_id}]; // [ [[픽 된 목록], [픽 안 된 목록]] ]
    for(var i = 0; i < banned_id.length; i++)
    {
        var currentBannedId = banned_id[i];

        var newCache = [];
        for(var j =0; j< cache.length; j++) {
            let currentCache = cache[j];

            for(var k = 0; k < currentCache.notPeek.length; k++) {
                let currentUserId = currentCache.notPeek[k];

                if(currentBannedId.length === currentUserId.length)
                {
                    for(var l = 0; l < currentBannedId.length; l++) {
                        if(currentBannedId[l] !== '*' && currentBannedId[l] !== currentUserId[l]) {
                            break;
                        }
                    }
                    if(l == currentBannedId.length) {
                        var newPeek = currentCache.peek.slice(0);
                        newPeek.push(currentUserId);
                        var newNotPeek = currentCache.notPeek.slice(0);
                        newNotPeek.splice(k,1);
                        newCache.push( { peek: newPeek, notPeek: newNotPeek } );
                    }
                }
            }
        }
        if(newCache.length === 0) {
            return 0;
        }
        cache = newCache;
    }

    var peekSet = new Set(cache.map(v => v.peek.sort().join(',')));

    return peekSet.size;
}

console.log(solution(["frodo", "fradi", "crodo", "abc123", "frodoc"],["fr*d*", "abc1**"],2));
console.log(solution(["frodo", "fradi", "crodo", "abc123", "frodoc"],["*rodo", "*rodo", "******"],2));
console.log(solution(["frodo", "fradi", "crodo", "abc123", "frodoc"],["fr*d*", "*rodo", "******", "******"],3));