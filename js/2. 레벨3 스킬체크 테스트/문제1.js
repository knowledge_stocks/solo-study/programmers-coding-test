function solution(n, weak, dist) {
    dist.sort((l,r) => r - l);

    for(var i = 0; i < dist.length; i++) {
        var idx = -1;
        var max = 0;
        var d = dist[i];
        for(var j = 0; j < n; j ++) {
            var c = weak.filter(w => {
                return d < ((w - j + n) % n);
            });

            if(c.length === 0) {
                idx = j;
                break;
            }

            var max_t = 0;
            c.forEach((v, i) => {
                max_t = Math.max(max_t, (i < c.length - 1 ? c[i + 1] : (c[0] + n)) - v);
            })

            if(max_t > max) {
                max = max_t;
                idx = j;
            }
        }
        weak = weak.filter(w => {
            return d < ((w - idx + n) % n);
        });

        console.log(idx, max, d, weak.length);
        if(weak.length === 0) {
            return i + 1;
        }
    }

    return -1;
}

console.log(solution(12,[1, 5, 6, 10],[1, 2, 3, 4],2));
console.log(solution(12,[1, 3, 4, 9, 10],[3, 5, 7],1));